# Import python libs
import os
import sys

# Import pop libs
from unittest.mock import patch

import pop.hub

# Import third aprty libs
import pytest

CPATH_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), "config")


def test_file():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--config")
    sys.argv.append(os.path.join(CPATH_DIR, "simple.conf"))
    hub.config.integrate.load(["f1"], "f1")
    assert hub.OPT["f1"]["test"] == "simple"
    assert hub.OPT["f1"]["error"] is False


def test_shebang():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--config")
    sys.argv.append(os.path.join(CPATH_DIR, "pipe.conf"))
    hub.config.integrate.load(["f1"], "f1")
    assert hub.OPT["f1"]["foo"] == "bar"


def test_file_default():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    hub.config.integrate.load(["f3"], "f3")
    assert hub.OPT["f3"]["cheese"] is True


def test_dir():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--config-dir")
    sys.argv.append(os.path.join(CPATH_DIR, "simple.d"))
    hub.config.integrate.load(["f2"], "f2")
    assert hub.OPT["f2"]["first"] is True
    assert hub.OPT["f2"]["second"] is True
    assert hub.OPT["f2"]["rocks"] is True
    assert hub.OPT["f2"]["seeds"] is True


def test_includes():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--config")
    sys.argv.append(os.path.join(CPATH_DIR, "include1.conf"))
    hub.config.integrate.load("f1", "f1")
    assert hub.OPT["f1"]["included"] is True
    assert hub.OPT["f1"]["test"] is True


def test_root_dir_noop():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    with patch(
        "pop_config.config.contracts.order.os.geteuid",
        autospec=True,
        return_value=0,
    ):
        hub.config.integrate.load(["r1"], "r1")
    assert hub.OPT["r1"]["cheese_dir"] == "/etc/r1/cheese"
    assert hub.OPT["r1"]["bacon_dir"] == "/etc/foo/bacon"


def test_root_dir_explicit():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--root-dir")
    sys.argv.append("myroot")
    with patch(
        "pop_config.config.contracts.order.os.geteuid",
        autospec=True,
        return_value=0,
    ):
        hub.config.integrate.load(["r1"], "r1")
    assert hub.OPT["r1"]["cheese_dir"] == "myroot/etc/r1/cheese"
    assert hub.OPT["r1"]["bacon_dir"] == "/etc/foo/bacon"


def test_root_dir_non_root_user():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    with patch(
        "pop_config.config.contracts.order.os.geteuid",
        autospec=True,
        return_value=12345,
    ):
        hub.config.integrate.load(["r1"], "r1")
    assert hub.OPT["r1"]["root_dir"] != "/"
